import mongoose, { Schema } from 'mongoose'

// Schema blueprint
const schema = new Schema(
  {
    title: String,
    description: String,
    done: Boolean,
  },
  { timestamps: { createdAt: 'createdAt' }, versionKey: false },
)

const Task = mongoose.model('Task', schema)

export { Task }
