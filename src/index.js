import configs from './configs/app'
import app from './routes/koa'
import { InitializeDB } from './db/db'

// Create Koa app
const startApp = () => {
  // Start the application
  app
    .listen(configs.NODE_PORT, () => {
      console.info(`Server listening on port: ${configs.NODE_PORT}`)
    })
    .on('Error', (err) => {
      console.error(err)
    })
}

// 1) InitializeDB
// 2) Start app
InitializeDB().then(startApp)

export default startApp
