import { task } from '../repository/index'
import * as utils from '../utils/utils'

async function listAllTasks(ctx) {
  const title = ctx.query.title
  const regEx = new RegExp(title, 'i')
  try {
    const allTasks = await task.queryAll({ title: regEx })
    const donePercent = utils.calculatePercentage(allTasks)

    ctx.body = {
      code: 200,
      data: { donePercent, allTasks },
      msg: 'Query all tasks successfully'
    }
  } catch (err) {
    ctx.throw(500, err)
  }
}

export default listAllTasks
