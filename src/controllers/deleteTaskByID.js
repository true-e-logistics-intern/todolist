import { task } from '../repository/index'
import { isEmpty } from 'lodash'

const expectedIDLength = 24

async function deleteTaskByID(ctx) {
  let id = ctx.params.id
  if (id.length != expectedIDLength) {
    ctx.throw(400, `id params(${id}) should be 24 chars long`)
  }

  let t = await task.queryByID(id)
  if (isEmpty(t)) {
    ctx.throw(404, `Task(${id}) Not Found!`)
  }

  try {
    await task.deleteByID(id)
    ctx.body = {
      code: 200,
      data: t,
      msg: 'Delete task by ID successfully'
    }
  } catch (error) {
    ctx.throw(500, 'Something error happens')
  }
}

export default deleteTaskByID
