import { task } from '../repository/index'
import { isEmpty } from 'lodash'

const expectedIDLength = 24

async function listTaskByID(ctx) {
  let { id } = ctx.params
  if (id.length != expectedIDLength) {
    ctx.throw(400, `id params(${id}) should be 24 chars long`)
  }
  let t = await task.queryByID(id)
  if (isEmpty(t)) {
    ctx.throw(404, `Task(${id}) Not Found!`)
  }

  ctx.status = 200
  ctx.body = {
    code: 200,
    data: t,
    msg: 'Query task by ID successfully'
  }
}

export default listTaskByID
