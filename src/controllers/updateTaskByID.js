import { task } from '../repository/index'
import { isEmpty } from 'lodash'

const expectedIDLength = 24

async function updateTaskByID(ctx) {
  const _id = ctx.params.id
  if (_id.length != expectedIDLength) {
    ctx.throw(400, `id params(${_id}) should be 24 chars long`)
  }
  
  const t = await task.queryByID(_id)
  if (isEmpty(t)) {
    ctx.throw(404, `Task(${_id}) Not Found!`)
  }

  const reqBody = ctx.request.body
  if (!reqBody.title || !reqBody.description) {
    ctx.throw(400, 'Missing required field values ⇢ title or description cannot empty!')
  }

  try {
    let resData = await task.updateByID({ _id }, reqBody)

    // Sculpt response
    ctx.body = {
      code: 200,
      data: resData,
      msg: 'Update task by ID successfully'
    }
  } catch (err) {
    ctx.throw(500, err)
  }
}

export default updateTaskByID
