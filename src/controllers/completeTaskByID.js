import { task } from '../repository/index'
import { isEmpty } from 'lodash'

async function completeTaskByID(ctx) {
  const reqBody = ctx.request.body
  let id = ctx.params.id
  const resData = await task.completeByID(id, reqBody)
  if (isEmpty(resData)) {
    ctx.throw(404, `Task(${id}) Not Found!`)
  }

  ctx.body = {
    code: 200,
    data: resData,
    msg: 'Complete task by ID successfully'
  }
}

export default completeTaskByID
