import { task } from '../repository/index'

async function createTasks(ctx) {
  let reqBody = ctx.request.body
  if (!reqBody.title || !reqBody.description) {
    ctx.throw(400, 'Missing required fields: title or description')
  }
  reqBody.done = false

  try {
    const t = await task.create(reqBody)
    ctx.status = 201
    ctx.body = {
      code: 201,
      data: t,
      msg: 'Create a task successfully'
    }
  } catch (err) {
    ctx.throw(500, err)
  }
}

export default createTasks
