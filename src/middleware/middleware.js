const handleErrors = async (ctx, next) => {
  try {
    await next()
  } catch (err) {
    // will only respond with JSON
    ctx.status = err.statusCode || err.status || 500
    ctx.body = {
      error: err.message,
    }
  }
}

export { handleErrors }
