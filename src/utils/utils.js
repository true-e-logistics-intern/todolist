const calculatePercentage = (tasks) => {
  const totalTasks = tasks.length
  let countOnlyTrue = 0
  tasks.forEach((element) => {
    if (element.done) countOnlyTrue++
  })

  return (countOnlyTrue * 100) / totalTasks
}

export { calculatePercentage }
