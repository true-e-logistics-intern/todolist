import * as dotenv from 'dotenv'
import path from 'path'

dotenv.config({ path: path.resolve(__dirname, `./${process.env.ENVIRONMENT}.env`) })
if (dotenv.error) {
  throw dotenv.error
}

const configs = {
  NODE_PORT: process.env.PORT || process.env.NODE_PORT,
  MONGO_URL: process.env.MONGO_URL,
  RECONNECT_DB_TIMEOUT: process.env.RECONNECT_DB_TIMEOUT
}

export default configs
