import { Task } from '../model/task'

export async function deleteByID(_id) {
  const filter = { _id }
  return await Task.findOneAndRemove(filter)
}

export async function updateByID(filter, dataUpdate) {
  // new flag will return modified document
  const option = { new: true }

  return await Task.findOneAndUpdate(filter, dataUpdate, option)
}

export async function completeByID(_id, done) {
  const filter = { _id }
  // new flag will return modified document
  const option = { new: true }

  return await Task.findByIdAndUpdate(filter, { $set: done }, option)
}

// Insert one new `Task` document
export async function create(data) {
  return await Task.create(data)
}

export async function queryAll(query) {
  return await Task.find(query).sort({ done: 1 })
}

export async function queryByID(_id) {
  return await Task.findById(_id)
}
