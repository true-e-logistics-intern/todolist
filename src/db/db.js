import mongoose from 'mongoose'
import configs from '../configs/app'
import mongooseOption from './options'
import _ from './events'

export async function InitializeDB() {
  // Start DB connection!
  try {
    await mongoose.connect(configs.MONGO_URL, mongooseOption)
    return mongoose
  } catch (error) {
    console.log(`Setting failed to connect to ${configs.MONGO_URL}\n`, error.message)
    process.exit(1)
  }
}
