const mongooseOption = {
  useNewUrlParser: true,
  useUnifiedTopology: true,
  auto_reconnect: true,
  useFindAndModify: false
}

export default mongooseOption
