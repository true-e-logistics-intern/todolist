import mongoose, { connect } from 'mongoose'
import configs from '../configs/app'
import mongooseOption from './options'

mongoose.Promise = Promise
const db = mongoose.connection

db.on('connecting', () => {
  console.info('Connecting to MongoDB...')
})

db.on('error', (error) => {
  console.error(`MongoDB connection error: ${error}`)
  mongoose.disconnect()
})

db.on('connected', () => {
  console.info('Connection Established')
})

db.once('open', () => {
  console.info('MongoDB connection opened!')
})

db.on('reconnected', () => {
  console.info('MongoDB reconnected!')
})

// db.on('disconnected', () => {
//   console.error(`MongoDB disconnected! Reconnecting in ${configs.RECONNECT_DB_TIMEOUT / 1000}s...`)
//   setTimeout(async () => {
//     await connect(configs.MONGO_URL, mongooseOption), configs.RECONNECT_DB_TIMEOUT
//   })
// })

export default db
