import app from '../routes/koa'
import supertest from 'supertest'
import http from 'http'
import { task } from '../repository/index'

const request = supertest(http.createServer(app.callback()))
jest.mock('../repository/task')

// 🔱Test cases
describe('healthcheck', () => {
  test('home route GET /', async (done) => {
    const response = await request.get('/')

    const wantStatusCode = 200
    const gotStatusCode = response.statusCode
    const wantPayload = { msg: 'Hello world' }
    const gotPayload = response.body

    expect(gotStatusCode).toBe(wantStatusCode)
    expect(gotPayload).toMatchObject(wantPayload)
    done()
  })
})

describe('Endpoint testing', () => {
  test('[POST /tasks] should createTask', async () => {
    const reqBody = {
      title: '(test)foo',
      description: '(test)foo'
    }
    task.create.mockResolvedValue()

    const response = await request.post('/api/tasks').send(reqBody)
    const wantStatusCode = 201
    const gotStatusCode = response.statusCode
    const wantPayload = {
      code: 201,
      msg: 'Create a task successfully'
    }
    const gotPayload = response.body

    expect(gotStatusCode).toBe(wantStatusCode)
    expect(gotPayload).toStrictEqual(wantPayload)
  })

  test('[GET /tasks] should listAllTasks', async () => {
    const mockAllTasks = [
      {
        _id: '5f4e946b547971609a357acc',
        title: 'foo01',
        description: 'foo01',
        done: false,
        createdAt: '2020-09-01T18:35:23.701Z',
        updatedAt: '2020-09-01T18:35:23.701Z'
      },
      {
        _id: '5f4e9472547971609a357acd',
        title: 'foo02',
        description: 'foo02',
        done: false,
        createdAt: '2020-09-01T18:35:30.301Z',
        updatedAt: '2020-09-01T18:35:30.301Z'
      },
      {
        _id: '5f4e9479547971609a357ace',
        title: 'foo03',
        description: 'foo03',
        done: false,
        createdAt: '2020-09-01T18:35:37.368Z',
        updatedAt: '2020-09-01T18:35:37.368Z'
      }
    ]
    task.queryAll.mockResolvedValue(mockAllTasks)

    const response = await request.get('/api/tasks')
    const wantStatusCode = 200
    const gotStatusCode = response.statusCode
    const wantPayload = {
      code: 200,
      data: {
        donePercent: 0,
        allTasks: mockAllTasks
      },
      msg: 'Query all tasks successfully'
    }
    const gotPayload = response.body

    expect(gotStatusCode).toBe(wantStatusCode)
    expect(gotPayload).toStrictEqual(wantPayload)
  })

  test('[GET /tasks/:id] listTaskByID should successful✅', async () => {
    const mockTaskByID = {
      _id: '5f4e946b547971609a357acc',
      title: 'foo01',
      description: 'foo01',
      done: false,
      createdAt: '2020-09-01T18:35:23.701Z',
      updatedAt: '2020-09-01T18:35:23.701Z'
    }

    task.queryByID.mockResolvedValue(mockTaskByID)

    const response = await request.get('/api/tasks/5f4e946b547971609a357acc')
    const wantStatusCode = 200
    const gotStatusCode = response.statusCode
    const wantPayload = {
      code: 200,
      data: mockTaskByID,
      msg: 'Query task by ID successfully'
    }
    const gotPayload = response.body

    expect(gotStatusCode).toBe(wantStatusCode)
    expect(gotPayload).toStrictEqual(wantPayload)
  })

  test('[GET /tasks/:id] listTaskByID invalid params ID should fail❌', async () => {
    const invalidID = '5aaa'
    const response = await request.get(`/api/tasks/${invalidID}`)
    const wantStatusCode = 400
    const gotStatusCode = response.statusCode
    const wantPayload = {
      error: `id params(${invalidID}) should be 24 chars long`
    }
    const gotPayload = response.body

    expect(gotStatusCode).toBe(wantStatusCode)
    expect(gotPayload).toStrictEqual(wantPayload)
  })

  test('[GET /tasks/:id] listTaskByID  but task not found!, should fail❌', async () => {
    const mockUpdatTaskByID = {}
    task.queryByID.mockResolvedValue(mockUpdatTaskByID)

    const notFoundID = '5f4e946b547971609a357acc'
    const response = await request.get(`/api/tasks/${notFoundID}`)
    const wantStatusCode = 404
    const gotStatusCode = response.statusCode
    const wantPayload = {
      error: `Task(${notFoundID}) Not Found!`
    }
    const gotPayload = response.body

    expect(gotStatusCode).toBe(wantStatusCode)
    expect(gotPayload).toStrictEqual(wantPayload)
  })

  test('[PATCH /tasks/:id] updateTaskByID should successful✅', async () => {
    const reqBody = {
      title: 'foo01(updated)',
      description: 'foo01(updated)'
    }
    const mockTaskByID = {
      _id: '5f4e946b547971609a357acc',
      title: 'foo01',
      description: 'foo01',
      done: false,
      createdAt: '2020-09-01T18:35:23.701Z',
      updatedAt: '2020-09-01T18:35:23.701Z'
    }
    const mockUpdatTaskByID = {
      _id: '5f4e946b547971609a357acc',
      title: 'foo01(updated)',
      description: 'foo01(updated)',
      done: false,
      createdAt: '2020-09-01T18:35:23.701Z',
      updatedAt: '2020-09-01T18:35:23.701Z'
    }

    task.queryByID.mockResolvedValue(mockTaskByID)
    task.updateByID.mockResolvedValue(mockUpdatTaskByID)

    const response = await request.patch('/api/tasks/5f4e946b547971609a357acc').send(reqBody)
    const wantStatusCode = 200
    const gotStatusCode = response.statusCode
    const wantPayload = {
      code: 200,
      data: mockUpdatTaskByID,
      msg: 'Update task by ID successfully'
    }
    const gotPayload = response.body

    expect(gotStatusCode).toBe(wantStatusCode)
    expect(gotPayload).toStrictEqual(wantPayload)
  })

  test('[PATCH /tasks/:id] updateTaskByID request field values cannot empty, should fail❌', async () => {
    const reqBody = {
      title: '',
      description: ''
    }

    const response = await request.patch('/api/tasks/5f4e946b547971609a357acc').send(reqBody)
    const wantStatusCode = 400
    const gotStatusCode = response.statusCode
    const wantPayload = {
      error: 'Missing required field values ⇢ title or description cannot empty!'
    }
    const gotPayload = response.body

    expect(gotStatusCode).toBe(wantStatusCode)
    expect(gotPayload).toStrictEqual(wantPayload)
  })

  test('[PATCH /tasks/:id] updateTaskByID invalid params ID, should fail❌', async () => {
    const invalidID = '5aaa'
    const response = await request.patch(`/api/tasks/${invalidID}`)
    const wantStatusCode = 400
    const gotStatusCode = response.statusCode
    const wantPayload = {
      error: `id params(${invalidID}) should be 24 chars long`
    }
    const gotPayload = response.body

    expect(gotStatusCode).toBe(wantStatusCode)
    expect(gotPayload).toStrictEqual(wantPayload)
  })

  test('[PATCH /tasks/:id] updateTaskByID but task not found!, should fail❌', async () => {
    const mockUpdatTaskByID = {}
    task.queryByID.mockResolvedValue(mockUpdatTaskByID)

    const notFoundID = '5f4e946b547971609a357acc'
    const response = await request.patch(`/api/tasks/${notFoundID}`)
    const wantStatusCode = 404
    const gotStatusCode = response.statusCode
    const wantPayload = {
      error: `Task(${notFoundID}) Not Found!`
    }
    const gotPayload = response.body

    expect(gotStatusCode).toBe(wantStatusCode)
    expect(gotPayload).toStrictEqual(wantPayload)
  })

  test('[DELETE /tasks/:id] deleteTaskByID ⇨ should successful✅', async () => {
    const mockTaskByID = {
      _id: '5f4e946b547971609a357acc',
      title: 'foo01',
      description: 'foo01',
      done: false,
      createdAt: '2020-09-01T18:35:23.701Z',
      updatedAt: '2020-09-01T18:35:23.701Z'
    }

    task.queryByID.mockResolvedValue(mockTaskByID)
    task.deleteByID.mockResolvedValue(mockTaskByID)
    const response = await request.delete(`/api/tasks/5f4e9479547971609a357ace`)

    const wantStatusCode = 200
    const gotStatusCode = response.statusCode
    const wantPayload = {
      code: 200,
      data: mockTaskByID,
      msg: 'Delete task by ID successfully'
    }
    const gotPayload = response.body

    expect(gotStatusCode).toBe(wantStatusCode)
    expect(gotPayload).toStrictEqual(wantPayload)
  })

  test('[DELETE /tasks/:id] deleteTaskByID but task not found! ⇨ should fail❌', async () => {
    const mockQueryTaskByID = {}
    task.queryByID.mockResolvedValue(mockQueryTaskByID)

    const notFoundID = '5f4e946b547971609a357acc'
    const response = await request.delete(`/api/tasks/${notFoundID}`)
    const wantStatusCode = 404
    const gotStatusCode = response.statusCode
    const wantPayload = {
      error: `Task(${notFoundID}) Not Found!`
    }
    const gotPayload = response.body

    expect(gotStatusCode).toBe(wantStatusCode)
    expect(gotPayload).toStrictEqual(wantPayload)
  })

  test('[DELETE /tasks/:id] deleteTaskByID but invalid params ID ⇨ should fail❌', async () => {
    const invalidID = '5aaa'
    const response = await request.delete(`/api/tasks/${invalidID}`)
    const wantStatusCode = 400
    const gotStatusCode = response.statusCode
    const wantPayload = {
      error: `id params(${invalidID}) should be 24 chars long`
    }
    const gotPayload = response.body

    expect(gotStatusCode).toBe(wantStatusCode)
    expect(gotPayload).toStrictEqual(wantPayload)
  })
})
