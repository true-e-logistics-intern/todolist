import * as utils from '../utils/utils'

test('Input 5 tasks ⇨ should return done 60%', () => {
  const mockAllTasks = [
    {
      title: 'foo01',
      description: 'foo01',
      done: false
    },
    {
      title: 'foo02',
      description: 'foo02',
      done: false
    },
    {
      title: 'foo03',
      description: 'foo03',
      done: true
    },
    {
      title: 'foo04',
      description: 'foo04',
      done: true
    },
    {
      title: 'foo05',
      description: 'foo05',
      done: true
    }
  ]
  const result = utils.calculatePercentage(mockAllTasks)

  expect(result).toEqual(60)
})
