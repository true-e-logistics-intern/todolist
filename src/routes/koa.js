import Koa from 'koa'
import logger from 'koa-logger'
import bodyParser from 'koa-bodyparser'
import cors from '@koa/cors'
import route from './route'
import * as middleware from '../middleware/middleware'

// Create Koa instance
const app = new Koa()
app
  .use(middleware.handleErrors)
  .use(logger())
  .use(bodyParser())
  .use(cors())
  .use(route.routes())
  .use(route.allowedMethods())
  
export default app