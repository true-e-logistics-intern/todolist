# To-do list API (Node.js with Koa)

A barebones Node.js app using [Koa](https://www.npmjs.com/package/koa).

## Running Locally

Make sure you have [Node.js](http://nodejs.org/) and the [Mongo DB](https://www.mongodb.com/)

```sh
$ git clone https://gitlab.com/true-e-logistics-intern/todolist_react.git
$ cd todolist
$ npm install
$ npm run dev
```

Your app should now be running on [localhost:8080](http://localhost:8080/)

<!-- ## Deploying to Heroku

```
$ heroku create
$ git push heroku master
$ heroku open
```

or

[![Deploy to Heroku](https://www.herokucdn.com/deploy/button.png)](https://heroku.com/deploy) -->

## Project Todolist Structure

```
.
├── node_modules
├── scr
|   ├──confiigs
|   |   └── app.js
|   ├──controllers
|   |   ├── completeTaskByID.js
|   |   ├── createTask.js
|   |   ├── deleteTaskByID.js
|   |   ├── index.js
|   |   ├── listAllTasks.js
|   |   ├── listTaskByID.js
|   |   └── updateTaskByID.js
|   ├──db
|   |   └── db.js
|   ├──model
|   |   └── task.js
|   ├──repository
|   |   ├── index.js
|   |   └── task.js
|   └──routes
|       └── route.js
├── .babelrc
├── .env
├── .gitignore
├── docker-compose.yml
├── package.json
├── package-lock.json
└── Procfile

```

## List of Packages

| Package         | Description                                           |
| --------------- | ----------------------------------------------------- |
| koa             | Node.js web framework                                 |
| koa-bodyparser  | A body parser for koa                                 |
| koa-Logger      | Logger middleware for koa                             |
| koa-router      | Router middleware for koa                             |
| mongoose        | MongoDB ODM                                           |
| prettier        | Prettier is an opinionated code formatter             |
| dotenv          | Loads environment variables from .env file            |
| nodemon         | Automatically restart Node.js server on code changes. |
| mongoose        | MongoDB ODM                                           |
| @types/jest     | JavaScript ES6                                        |
| devDependencies | To add dependencies and Dependencies.                 |
| babel/cli       | Babel is a JavaScript compiler                        |
| eslint          | Automated code review for github pull requests        |
